FROM python:3.8-slim

RUN pip3 install requests \
    BeautifulSoup4
WORKDIR /opt/web-scrappers
COPY . /opt/web-scrappers
CMD [ "python3", "/opt/web-scrappers/Luxoft/HoeWarmIsHetInDelft.py"]