#!/usr/bin/python3
import requests
import urllib.request
import time
from bs4 import BeautifulSoup

url ='https://weerindelft.nl/WU/55ajax-dashboard-testpage.php'
response = requests.get(url)

soup = BeautifulSoup(response.text, 'html.parser')
test_string = soup.find(id="ajaxtemp").text.strip()
test_string = test_string[:-2]

print(str(round(float(test_string)))+" degrees Celsius" )